#include "scene.h"

Scene::Scene(const Color &background): _background(background) {}

VisibleObject* Scene::operator [] (unsigned index)
{
    return objects[index];
}

const VisibleObject *Scene::operator [](unsigned index) const
{
    return objects[index];
}

void Scene::add(VisibleObject *object)
{
    objects.push_back(object);
}

unsigned Scene::size() const
{
    return objects.size();
}
