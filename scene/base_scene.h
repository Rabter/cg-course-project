#ifndef BASE_SCENE_H
#define BASE_SCENE_H

#include "model/visible_object.h"

class VisibleObject;

class BaseScene
{
public:
    virtual VisibleObject* operator [] (unsigned) = 0;
    virtual const VisibleObject* operator [] (unsigned) const = 0;
    virtual void add (VisibleObject *object) = 0;
    virtual unsigned size() const = 0;
};

#endif // BASE_SCENE_H
