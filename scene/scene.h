#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include "base_scene.h"
#include "light/lights.h"
#include "color/color.h"

class Scene: public BaseScene
{
public:
    Scene(const Color &background = 0xffffffff);

    VisibleObject* operator [] (unsigned index) override;
    const VisibleObject* operator [] (unsigned index) const override;

    inline void set_background(Color new_background)
    { _background = new_background; }
    inline Color background() const
    { return _background; }

    void add(VisibleObject *object) override;
    unsigned size() const override;

    Lights lights;
private:
    std::vector<VisibleObject*> objects;
    Color _background;
};

#endif // SCENE_H
