#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include "scene/scene.h"
#include "camera/camera.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_render_clicked();
    void on_dial_camera_horizontal_valueChanged(int value);
    void on_slider_camera_vertical_valueChanged(int value);
    void on_slider_camera_zoom_valueChanged(int value);
    void on_dial_light_horizontal_valueChanged(int value);
    void on_slider_light_vertical_valueChanged(int value);
    void on_rb_point_light_toggled(bool checked);
    void on_rb_direct_light_toggled(bool checked);
    void on_slider_light_intensity_valueChanged(int value);

private:
    void initialize_scene();
    void update_result();
    void set_info_done();

    void rotate_light_horizontal(int value);
    void rotate_light_vertical(int value);
    void rotate_camera_horizontal(int value);
    void rotate_camera_vertical(int value);

    Ui::MainWindow *ui;
    Scene scene;
    Camera *camera;
    Point scene_center;
    int camera_horizontal_angle, camera_vertical_angle;
    int light_horizontal_angle, light_vertical_angle;

private slots:
    void obsolete_handler();

    void on_slider_light_point_zoom_valueChanged(int value);

signals:
    void result_is_obsolete();
};

#endif // MAINWINDOW_H
