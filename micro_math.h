#ifndef MICRO_MATH_H
#define MICRO_MATH_H

template <class T>
inline constexpr T sqr(const T &a) {return a * a; }
template <class T>
inline constexpr T min(const T &a, const T &b) { return a < b? a : b; }
template <class T>
inline constexpr T  max(const T &a, const T &b) { return a > b? a : b; }
template <class T>
inline constexpr T templ_abs(const T &a) { return a > 0? a : -a; }

#endif // MICRO_MATH_H
