#include "visible_circle.h"
#include "micro_math.h"

#define EPS 1e-10
VisibleCircle::VisibleCircle(const Circle &shape, const Color &color, double brilliance, double reflectivity, double transperancy):
    brilliance(brilliance), reflectivity(reflectivity), transperancy(transperancy), shape(shape), color(color)
{
    double radius = shape.radius();
    sphere_tester = new SphereRayTester(shape.center(), radius);
    Parallelepiped rect_shape(Point(-radius, -radius, -0.5), Point(radius, radius, 0.5));
    Point tmp_vector(shape.normal());
    if (templ_abs<double>(tmp_vector.y()) > EPS)
        rect_shape.rotate_x(templ_abs<double>(tmp_vector.z()) < EPS? M_PI_2 : atan(tmp_vector.y() / tmp_vector.z()));
    if (templ_abs<double>(tmp_vector.x()) > EPS)
        rect_shape.rotate_y(templ_abs<double>(tmp_vector.z()) < EPS? M_PI_2 : atan(tmp_vector.x() / tmp_vector.z()));
    rect_shape.move(shape.center());
    rect_tester = new RectRayTester(rect_shape);
}
#undef EPS

VisibleCircle::~VisibleCircle()
{
    delete sphere_tester;
    delete rect_tester;
}

bool VisibleCircle::find(double &intersection_t, Color &color, Point &normal, double &brilliance, double &reflectivity, double &transperancy, const Ray &ray, double t_min) const
{
    if (!test(ray))
        return false;
    Point a, b, c;
    shape.plane_points(a, b, c);
    if (!ray.intersects_plane(intersection_t, a, b, c))
        return false;
    if (intersection_t < t_min || (shape.center() - ray.point_at(intersection_t)).length() > shape.radius())
        return false;
    normal = (a - b) ^ (a - c);
    normal /= normal.length();
    if (normal * ray.direction().RVector() > 0)
        normal = normal * -1;
    color = this->color;
    brilliance = this->brilliance;
    reflectivity = this->reflectivity;
    transperancy = this->transperancy;
    return true;
}

bool VisibleCircle::has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const
{
    if (!test(ray))
        return false;
    Point a, b, c;
    shape.plane_points(a, b, c);
    double intersection_t;
    if (!ray.intersects_plane(intersection_t, a, b, c))
        return false;
    if (intersection_t < t_min || intersection_t > t_max || (shape.center() - ray.point_at(intersection_t)).length() > shape.radius())
        return false;
    transperancy = this->transperancy;
    return true;
}

bool VisibleCircle::test(const Ray &ray) const
{
    if (!sphere_tester->test(ray.direction()))
        return false;
//    if (!rect_tester->test(ray.direction()))
//        return false;
    return true;
}

