#include <cmath>
#include "micro_math.h"
#include "visible_pipe.h"

#define EPS 1e-10
VisiblePipe::VisiblePipe(const Cylinder &shape, const Color &color, double brilliance, double reflectivity, double transperancy):
    brilliance(brilliance), reflectivity(reflectivity), transperancy(transperancy), shape(shape), color(color), AB(shape.base2() - shape.base1())
{
    double pipe_length = (shape.base2() - shape.base1()).length(), pipe_rad = shape.radius();
    double sphere_radius = sqrt(sqr(pipe_length / 2) + sqr(pipe_rad));
    sphere_tester = new SphereRayTester((shape.base1() + shape.base2()) / 2, sphere_radius);
    Parallelepiped rect_shape(Point(-pipe_rad, -pipe_rad, 0), Point(pipe_rad, pipe_rad, pipe_length));
    Point tmp_vector = shape.base2() - shape.base1();
    if (templ_abs<double>(tmp_vector.y()) > EPS)
        rect_shape.rotate_x(templ_abs<double>(tmp_vector.z()) < EPS? M_PI_2 : atan(tmp_vector.y() / tmp_vector.z()));
    if (templ_abs<double>(tmp_vector.x()) > EPS)
        rect_shape.rotate_y(templ_abs<double>(tmp_vector.z()) < EPS? M_PI_2 : atan(tmp_vector.x() / tmp_vector.z()));
    rect_shape.move(shape.base1());
    rect_tester = new RectRayTester(rect_shape);
}
#undef EPS

VisiblePipe::~VisiblePipe()
{
    delete sphere_tester;
    delete rect_tester;
}

bool VisiblePipe::find(double &intersection_t, Color &color, Point &normal, double &brilliance, double &reflectivity, double &transperancy, const Ray &ray, double t_min) const
{
    if (!test(ray))
        return false;
    // The distance from point M to the line given by points A and B is |AB^AM| / |AB| from the geometric sense of the vector product
    // Next actions are similar to 'find' for sphere
    Point AM;
    Point AC = ray.direction().first() - shape.base1();
    Point atmp = AB ^ ray.direction().RVector(), bctmp = AB ^ AC;
    double radius = shape.radius();
    double a = atmp * atmp;
    double b = 2 * (bctmp * (AB ^ ray.direction().RVector()));
    double c = (bctmp * bctmp) - radius * radius * AB.length() * AB.length();
    double discriminant = b * b - 4 * a * c;
    double ts;
    if (discriminant < 0)
        return false;
    else
    {
        discriminant = sqrt(discriminant);
        ts = (-b - discriminant) / (2 * a);
        AM = ray.point_at(ts) - shape.base1();
        double projection_length = AM * AB / AB.length();
        if (ts < t_min || projection_length > AB.length() || projection_length < 0)
        {
            ts = (-b + discriminant) / (2 * a);
            AM = ray.point_at(ts) - shape.base1();
            projection_length = AM * AB / AB.length();
            if (ts < t_min || projection_length > AB.length() || projection_length < 0)
                return false;
        }
    }

    intersection_t = ts;
    color = this->color;
    brilliance = this->brilliance;
    reflectivity = this->reflectivity;
    transperancy = this->transperancy;
    normal = (AM ^ AB) ^ AB;
    normal /= normal.length();
    if (normal * ray.direction().RVector() > 0)
        normal = normal * -1;

    return true;
}

bool VisiblePipe::has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const
{
    if (!test(ray))
        return false;
    Point AC = ray.direction().first() - shape.base1();
    Point atmp = AB ^ ray.direction().RVector(), bctmp = AB ^ AC;
    double radius = shape.radius();
    double a = atmp * atmp;
    double b = 2 * (bctmp * (AB ^ ray.direction().RVector()));
    double c = (bctmp * bctmp) - radius * radius * AB.length() * AB.length();
    double discriminant = b * b - 4 * a * c;
    if (discriminant < 0)
        return false;
    else
    {
        bool found = false;
        discriminant = sqrt(discriminant);
        double intersection_t = (-b - discriminant) / (2 * a);
        Point AM = ray.point_at(intersection_t) - shape.base1();
        double projection_length = AM * AB / AB.length();
        if (intersection_t >= t_min && intersection_t <= t_max && projection_length <= AB.length() && projection_length >= 0)
        {
            transperancy *= this->transperancy;
            found = true;
        }
        intersection_t = (-b + discriminant) / (2 * a);
        AM = ray.point_at(intersection_t) - shape.base1();
        projection_length = AM * AB / AB.length();
        if (intersection_t >= t_min && intersection_t <= t_max && projection_length <= AB.length() && projection_length >= 0)
        {
            transperancy *= this->transperancy;
            found = true;
        }
        return found;
    }
}

bool VisiblePipe::test(const Ray &ray) const
{
    if (!sphere_tester->test(ray.direction()))
        return false;
//    if (!rect_tester->test(ray.direction()))
//        return false;
    return true;
}
