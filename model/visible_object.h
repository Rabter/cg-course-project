#ifndef VISIBLE_OBJECT_H
#define VISIBLE_OBJECT_H

#include "ray/ray.h"
#include "color/color.h"
#include "geometry/geometric_primitives.h"


class Ray;

class VisibleObject
{
public:
    VisibleObject() = default;
    virtual ~VisibleObject() = default;
    virtual bool find(double &intersection_t, Color &color, Point &normal, double &brilliance, double &reflectivity, double &transperancy, const Ray &ray, double t_min) const = 0;
    virtual bool has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const = 0;
    virtual bool test(const Ray &ray) const = 0;
};

#endif // VISIBE_LOBJECT_H
