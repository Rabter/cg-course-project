#include "model.h"

bool Model::find(double &intersection_t, Color &color, Point &normal, double &brilliance, double &reflectivity, double &transperancy, const Ray &ray, double t_min) const
{
    if (!test(ray))
        return false;
    Color tmpcolor;
    Point tmpnormal;
    double tmpt, tmpbrill, tmprefl, tmptran;
    bool tmin_is_undefined = true;
    unsigned size = objects.size();
    for (unsigned i = 0; i < size; ++i)
    {
        if (objects[i]->find(tmpt, tmpcolor, tmpnormal, tmpbrill, tmprefl, tmptran, ray, t_min) && (tmin_is_undefined || tmpt < intersection_t))
        {
            intersection_t = tmpt;
            color = tmpcolor;
            normal = tmpnormal;
            brilliance = tmpbrill;
            reflectivity = tmprefl;
            transperancy = tmptran;
            tmin_is_undefined = false;
        }
    }
    return !tmin_is_undefined;
}

bool Model::has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const
{
    unsigned size = objects.size();
    double tmp;
    bool found = false;
    transperancy = 1;
    for (unsigned i = 0; i < size; ++i)
    {
        if (objects[i]->has_intersection(tmp, ray, t_min, t_max))
        {
            transperancy *= tmp;
            found = true;
        }
    }
    return found;
}

bool Model::test(const Ray &ray) const
{
    unsigned size = objects.size(), i = 0;
    for (i = 0; i < size; ++i)
        if (objects[i]->test(ray))
            return true;
    return false;
}

void Model::append(VisibleObject *object)
{
    objects.push_back(object);
}

void Model::destroy()
{
    for (VisibleObject *object : objects)
        delete object;
}
