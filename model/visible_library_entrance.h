#ifndef VISIBLELIBRARYENTRANCE_H
#define VISIBLELIBRARYENTRANCE_H

#include "model.h"

class VisibleLibraryEntrance: public Model
{
public:
    VisibleLibraryEntrance(const Point &spawn_point);
    ~VisibleLibraryEntrance();

private:
    static constexpr double inner_width = 60;
    static constexpr double front_side_height = 95;
    static constexpr double lateral_side_height = 95;
    static constexpr double front_side_top_length = 80, front_side_bottom_length = 80, lateral_side_top_length = 100, lateral_side_bottom_length = 150;
    static constexpr double common_side_length_squared = sqr<double>(lateral_side_bottom_length - lateral_side_top_length) + lateral_side_height * lateral_side_height;
    static constexpr double inner_top_length = lateral_side_top_length/* * 4.0 / 5*/, inner_bottom_length = lateral_side_bottom_length/* * 4.0 / 5*/;
    static constexpr double front_side_angle = 0;//M_PI / 6;
    const double lateral_side_angle = 0;

};

#endif // VISIBLELIBRARYENTRANCE_H
