#ifndef GLASS_H
#define GLASS_H

#include "color/color.h"
constexpr double glass_brilliance = 100, glass_reflectivity = 0.65, glass_transperancy = 0.45;
const Color glass_color = 0xff000055;

#endif // GLASS_H
