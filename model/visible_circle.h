#ifndef VISIBLE_CIRCLE_H
#define VISIBLE_CIRCLE_H

#include "visible_object.h"
#include "geometry/geometric_objects.h"
#include "geometry/ray_trace_testers.h"

class VisibleCircle: public VisibleObject
{
public:
    VisibleCircle(const Circle &shape, const Color &color, double brilliance = -1, double reflectivity = -1, double transperancy = 0);
    ~VisibleCircle();

    virtual bool find(double &intersection_t, Color &color, Point &normal, double &brilliance, double &reflectivity, double &transperancy, const Ray &ray, double t_min) const override;
    virtual bool has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const override;
    virtual bool test(const Ray &ray) const override;

    double brilliance, reflectivity, transperancy;

protected:
    Circle shape;
    Color color;
    SphereRayTester *sphere_tester;
    RectRayTester *rect_tester;
};

#endif // VISIBLE_CIRCLE_H
