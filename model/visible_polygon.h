#ifndef VISIBLE_POLYGON_H
#define VISIBLE_POLYGON_H

#include "visible_object.h"
#include "geometry/geometric_objects.h"
#include "geometry/ray_trace_testers.h"

class VisiblePolygon: public VisibleObject
{
public:
    VisiblePolygon(const Polygon &shape, const Color &color, double brilliance = -1, double reflectivity = -1, double transperancy = 0);
    virtual ~VisiblePolygon();
    virtual bool find(double &intersection_t, Color &color, Point &normal, double &brilliance, double &reflectivity, double &transperancy, const Ray &ray, double t_min) const override;
    virtual bool has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const override;
    virtual bool test(const Ray &ray) const override;

    bool point_is_inside(const Point &point) const;

    double brilliance, reflectivity, transperancy;

protected:
    void create_testers();

    Polygon shape;
    Polygon2D flat_shape;
    double flatter_x_angle, flatter_y_angle;
    Color color;
    SphereRayTester *sphere_tester;
    RectRayTester *rect_tester;
};

#endif // VISIBLE_POLYGON_H
