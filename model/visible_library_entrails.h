#ifndef VISIBLELIBRARYENTRAILS_H
#define VISIBLELIBRARYENTRAILS_H

#include "model.h"
#include "geometry/geometric_primitives.h"

class VisibleLibraryEntrails: public Model
{
public:
    VisibleLibraryEntrails(const Point &center, double side_length);
    ~VisibleLibraryEntrails();
    bool has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const override;
};

#endif // VISIBLELIBRARYENTRAILS_H
