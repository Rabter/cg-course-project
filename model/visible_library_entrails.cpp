#include <cmath>
#include "visible_library_entrails.h"
#include "geometry/geometric_objects.h"
#include "visible_polygon.h"

VisibleLibraryEntrails::VisibleLibraryEntrails(const Point &center, double side_length)
{
    std::vector<Rectangle> windows;
    const double half_side_length = side_length / 2;
    const double edge_dist = half_side_length + side_length / sqrt(2);
    const double hproportion = 0.8;
    const double wproportion = hproportion / 2;
    const double win_height = side_length * hproportion / 8;
    const double win_hspace = side_length * (1 - hproportion) / 16;
    const double win_width = side_length * wproportion / 8;
    const double win_wspace = side_length * (1 - wproportion) / 16;
    const double win_wstep = side_length / 8;
    for (double current_x = 0; current_x < half_side_length; current_x += win_wstep)
        windows.push_back(Rectangle(Point(current_x + win_wspace, win_hspace, 0), Point(current_x + win_wspace + win_width, win_hspace + win_height, 0)));

    windows.push_back(Rectangle(Point(win_wspace, 3 * win_hspace + win_height, 0), Point(win_wspace + win_width,  3 * win_hspace + 2 * win_height, 0)));
    windows.push_back(Rectangle(Point(7 * win_wspace + 3 * win_width, 3 * win_hspace + win_height, 0), Point(7 * win_wspace + 4 * win_width,  3 * win_hspace + 2 * win_height, 0)));
    windows.push_back(Rectangle(Point(win_wspace, 5 * win_hspace + 2 * win_height, 0), Point(win_wspace + win_width,  5 * win_hspace + 3 * win_height, 0)));
    windows.push_back(Rectangle(Point(7 * win_wspace + 3 * win_width, 5 * win_hspace + 2 * win_height, 0), Point(7 * win_wspace + 4 * win_width,  5 * win_hspace + 3 * win_height, 0)));

    for (double current_x = 0; current_x < half_side_length; current_x += win_wstep)
        windows.push_back(Rectangle(Point(current_x + win_wspace, 7 * win_hspace + 3 * win_height, 0), Point(current_x + win_wspace + win_width, 7 * win_hspace + 4 * win_height, 0)));

    for (unsigned i = 0; i < 12; ++i)
    {
        windows[i].move(Point(0, 0, edge_dist));
        windows.push_back(windows[i]);
        windows[i].move(Point(-half_side_length, 0, 0));
    }
    for (unsigned i = 0; i < 24; ++i)
    {
        windows.push_back(windows[i]);
        windows[i].move(Point(0, -half_side_length, 0));
    }
    for (unsigned i = 1; i < 4; ++i)
    {
        for (unsigned j = 0; j < 48; ++j)
        {
            windows.push_back(windows[j]);
            windows[windows.size() - 1].rotate_y(i * M_PI_4);
        }
        for (unsigned j = 0; j < 48; ++j)
        {
            windows.push_back(windows[j]);
            windows[windows.size() - 1].rotate_y(-i * M_PI_4);
        }
    }

    for (unsigned i = 0; i < 336; ++i) // Jesus so many windows
        windows[i].move(center);

    for (const Rectangle &rect: windows)
        this->objects.push_back(new VisiblePolygon(rect, 0xff000000));
}

VisibleLibraryEntrails::~VisibleLibraryEntrails()
{
    destroy();
}

bool VisibleLibraryEntrails::has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const
{
    return false;
    (void)transperancy;
    (void)ray;
    (void)t_min;
    (void)t_max;
}
