#include "visible_polygon.h"

VisiblePolygon::VisiblePolygon(const Polygon &shape, const Color &color, double brilliance, double reflectivity, double transperancy):
    brilliance(brilliance), reflectivity(reflectivity), transperancy(transperancy), shape(shape), flat_shape(shape.to_polygon_2d(&flatter_x_angle, &flatter_y_angle)), color(color)
{
    create_testers();
}

VisiblePolygon::~VisiblePolygon()
{
    if (sphere_tester)
        delete sphere_tester;
    if (rect_tester)
        delete rect_tester;
}

bool VisiblePolygon::test(const Ray &ray) const
{
    if (!sphere_tester->test(ray.direction()))
        return false;
//    if (!rect_tester->test(ray.direction())) // ToDo: Not sure if it gives any advantage
//        return false;
    return true;
}

void VisiblePolygon::create_testers()
{
    double x_angle, y_angle, shift;
    Polygon2D rect_2d = shape.to_polygon_2d(&x_angle, &y_angle, &shift);
    Point2D point = rect_2d.get_point(0);
    Point center(shape.get_point(0));
    unsigned size = shape.size();
    double x = point.x(), y = point.y();
    double xmin = x, xmax = x, ymin = y, ymax = y;
    for (unsigned i = 1; i < size; ++i)
    {
        center += shape.get_point(i);
        point = rect_2d.get_point(i);
        x = point.x(), y = point.y();
        if (x < xmin)
            xmin = x;
        if (x > xmax)
            xmax = x;
        if (y < ymin)
            ymin = y;
        if (y > ymax)
            ymax = y;
    }
    center /= size;
    double radius = 0, dist;
    for (unsigned i = 0; i < size; ++i)
    {
        dist = (shape.get_point(i) - center).length();
        if (dist > radius)
            radius = dist;
    }
    Parallelepiped rect_shape(Point(xmin, ymin, shift - 0.5), Point(xmax, ymax, shift + 0.5));
    rect_shape.rotate_y(-y_angle);
    rect_shape.rotate_x(-x_angle);
    sphere_tester = new SphereRayTester(center, radius);
    rect_tester = new RectRayTester(rect_shape);
}

static inline char sign(double a)
{
    if (a < 0)
        return -1;
    else if (a > 0)
        return 1;
    else
        return 0; // Happens rarely
}

char check_edge(const Point &point, const Point &a, const Point &b)
{
    double ax = a.x() - point.x();
    double bx = b.x() - point.x();
    double ay = a.y() - point.y();
    double by = b.y() - point.y();
    char s = sign(ax * by - ay * bx);
    if ((ay < 0) ^ (by < 0))
    {
        if (by < 0)
            return s;
        return -s;
    }
    if (s == 0 && (ax == 0 && ay == 0) && ax * bx <= 0)
        return 0;
    return 1;
}

bool VisiblePolygon::has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const
{
    if (!test(ray))
        return false;
    double intersection_t;
    if (!ray.intersects_plane(intersection_t, shape.get_point(0), shape.get_point(1), shape.get_point(2)))
        return false;
    if (intersection_t < t_min || intersection_t > t_max || !point_is_inside(ray.point_at(intersection_t)))
        return false;
    transperancy = this->transperancy;
    return true;
}

bool VisiblePolygon::point_is_inside(const Point &p) const
{
    Point point(p);
    point.rotate_x(flatter_x_angle);
    point.rotate_y(flatter_y_angle);
    unsigned sizedec = flat_shape.size() - 1;
    unsigned count = 0;
    for (unsigned i = 0; i < sizedec; ++i)
    {
        if (check_edge(point, flat_shape.get_point(i), flat_shape.get_point(i + 1)) - 1 != 0)
            ++count;
    }
    if (check_edge(point, flat_shape.get_point(sizedec), flat_shape.get_point(0)) - 1 != 0)
        ++count;
    if (count % 2)
        return true;
    return false;
}

bool VisiblePolygon::find(double &intersection_t, Color &color, Point &normal, double &brilliance, double &reflectivity, double &transperancy, const Ray &ray, double t_min) const
{
    if (!test(ray))
        return false;
    if (!ray.intersects_plane(intersection_t, shape.get_point(0), shape.get_point(1), shape.get_point(2)))
        return false;
    if (intersection_t < t_min || !point_is_inside(ray.point_at(intersection_t)))
        return false;
    color = this->color;
    normal = (shape.get_point(0) - shape.get_point(1)) ^ (shape.get_point(0) - shape.get_point(2));
    normal /= normal.length();
    if (normal * ray.direction().RVector() > 0)
        normal = normal * -1;
    brilliance = this->brilliance;
    reflectivity = this->reflectivity;
    transperancy = this->transperancy;
    return true;
}
