#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include "visible_object.h"

class Model: public VisibleObject
{
public:
    Model() = default;
    virtual ~Model() = default;
    bool find(double &intersection_t, Color &color, Point &normal, double &brilliance, double &reflectivity, double &transperancy, const Ray &ray, double t_min) const override;
    bool has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const override;
    bool test(const Ray &ray) const override;
    void append(VisibleObject *object);
    virtual void destroy();

protected:
    std::vector<VisibleObject*> objects;
};

#endif // MODEL_H
