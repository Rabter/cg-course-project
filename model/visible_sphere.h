#ifndef VISIBLE_SPHERE_H
#define VISIBLE_SPHERE_H

#include "model/visible_object.h"
#include "geometry/geometric_primitives.h"
#include "geometry/ray_trace_testers.h"

class VisibleSphere: public VisibleObject
{
public:
    VisibleSphere(const Point &center, double radius, const Color &color, double brilliance = -1, double reflectivity = -1, double transperancy = 0);
    bool find(double &intersection_t, Color &color, Point &normal, double &brilliance, double &reflectivity, double &transperancy, const Ray &ray, double t_min) const override;
    bool has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const override;
    bool test(const Ray &ray) const override;
    inline void set_color(ARGB color)
    { this->color = color; }

    double brilliance, reflectivity, transperancy;

protected:
    SphereRayTester tester;
    Point center;
    double radius;
    Color color;
};

#endif // VISIBLE_SPHERE_H
