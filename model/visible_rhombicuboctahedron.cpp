#include "visible_rhombicuboctahedron.h"

VisibleRhombicuboctahedron::VisibleRhombicuboctahedron(const Rhombicuboctahedron &shape, const Color &color, double brilliance, double reflectivity, double transperancy):
    shape(shape), color(color)
{
    for (unsigned i = 0; i < 26; ++i)
        visible_parts[i] = new VisiblePolygon(shape.get_side(i), color, brilliance, reflectivity, transperancy);
    tester = new SphereRayTester(shape.center(), (shape.get_side(0).get_point(0) - shape.center()).length());
}

VisibleRhombicuboctahedron::~VisibleRhombicuboctahedron()
{
    for (unsigned i = 0; i < 26; ++i)
        delete visible_parts[i];
    delete tester;
}

bool VisibleRhombicuboctahedron::find(double &intersection_t, Color &color, Point &normal, double &brilliance, double &reflectivity, double &transperancy, const Ray &ray, double t_min) const
{
    if (!test(ray))
        return false;
    Color tmpcolor;
    Point tmpnormal;
    double tmpt, tmpbrill, tmprefl, tmptran;
    bool tmin_is_undefined = true;
    for (unsigned i = 0; i < 26; ++i)
    {
        if (visible_parts[i]->find(tmpt, tmpcolor, tmpnormal, tmpbrill, tmprefl, tmptran, ray, t_min) && (tmin_is_undefined || tmpt < intersection_t))
        {
            intersection_t = tmpt;
            color = tmpcolor;
            normal = tmpnormal;
            brilliance = tmpbrill;
            reflectivity = tmprefl;
            transperancy = tmptran;
            tmin_is_undefined = false;
        }
    }
    return !tmin_is_undefined;
}

bool VisibleRhombicuboctahedron::has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const
{
    double tmp;
    bool found = false;
    transperancy = 1;
    for (unsigned i = 0; i < 26; ++i)
    {
        if (visible_parts[i]->has_intersection(tmp, ray, t_min, t_max))
        {
            transperancy *= tmp;
            found = true;
        }
    }
    return found;
}

bool VisibleRhombicuboctahedron::test(const Ray &ray) const
{
    return tester->test(ray.direction());
}
