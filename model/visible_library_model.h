#ifndef VISIBLE_LIBRARY_MODEL_H
#define VISIBLE_LIBRARY_MODEL_H

#include "model.h"
#define SQRT_2 1.4142135623730951 // result of sqrt(2)
// Real library total height is 76.67m

class VisibleLibraryModel: public Model
{
public:
    VisibleLibraryModel(Point spawn_point);
    ~VisibleLibraryModel();

private:
    static constexpr double head_side_length = 100, head_side_halflength = head_side_length / 2;
    static constexpr double edge_dist = head_side_halflength + head_side_length / SQRT_2;
    static constexpr double base1_height = 35, base2_height = 30, base3_height = 30, mirror1_part = 4.0 / 5, mirror2_part = 4.0 / 5, mirror3_part = 4.0 / 5;
    static constexpr  double base1_radius = 250, base2_radius = 225, base3_radius = 200;
    const Point base1_shift, base2_shift, base3_shift;
    const Point head_shift;
    const Point head_center;
};
#undef SQRT_2

#endif // VISIBLE_LIBRARY_MODEL_H
