#include "visible_library_entrance.h"
#include "geometry/geometric_objects.h"
#include "visible_polygon.h"
#include "glass.h"

VisibleLibraryEntrance::VisibleLibraryEntrance(const Point &spawn_point)
{
    Polygon entrance_right(std::vector<Point2D>({Point2D(0,0), Point2D(0,lateral_side_height), Point2D(lateral_side_top_length,lateral_side_height), Point2D(lateral_side_bottom_length,0)}));
    entrance_right.rotate_y(-M_PI_2);
    Polygon entrance_left(entrance_right);
    entrance_left.rotate_z(lateral_side_angle);
    entrance_right.rotate_z(-lateral_side_angle);
    entrance_right.move(spawn_point + Point(inner_width / 2 + front_side_top_length * cos(front_side_angle), 0, 0));
    entrance_left.move(spawn_point + Point(-inner_width / 2 - front_side_top_length * cos(front_side_angle), 0, 0));

    objects.push_back(new VisiblePolygon(entrance_right, glass_color, glass_brilliance, 0.3));
    objects.push_back(new VisiblePolygon(entrance_left, glass_color, glass_brilliance, 0.3));

    Polygon entrance_inner(std::vector<Point2D>({Point2D(0,0), Point2D(0,lateral_side_height), Point2D(inner_top_length,lateral_side_height), Point2D(inner_bottom_length,0)}));
    entrance_inner.rotate_y(-M_PI_2);

    entrance_inner.move(spawn_point + Point(inner_width / 2, 0, 0));
    objects.push_back(new VisiblePolygon(entrance_inner, 0xffffffff, 1000, 0.2));
    entrance_inner.move(Point(-inner_width, 0, 0));
    objects.push_back(new VisiblePolygon(entrance_inner, 0xffffffff, 1000, 0.2));

    const double angle = acos((lateral_side_bottom_length - lateral_side_top_length) / sqrt(common_side_length_squared));
    Polygon face(std::vector<Point2D>({Point2D(0,0), Point2D(0,lateral_side_height / sin(angle)), Point2D(front_side_top_length,lateral_side_height / sin(angle)), Point2D(front_side_bottom_length,0)}));
    face.rotate_x(M_PI_2 - angle);
    face.rotate_y(-front_side_angle);
    face.move(spawn_point + Point(inner_width / 2, 0, inner_bottom_length));
    objects.push_back(new VisiblePolygon(face, glass_color, glass_brilliance, 0.6, 0.1));
    face.move(Point(-inner_width - front_side_top_length * cos(front_side_angle), 0, 0));
    objects.push_back(new VisiblePolygon(face, glass_color, glass_brilliance, 0.6, 0.1));

    Polygon hat(std::vector<Point2D>({Point2D(0,0), Point2D(front_side_top_length, 0), Point2D(front_side_top_length, lateral_side_top_length), Point2D(0, lateral_side_top_length)}));
    hat.rotate_x(-M_PI_2);
    hat.move(spawn_point + Point(inner_width / 2, lateral_side_height, 0));
    objects.push_back(new VisiblePolygon(hat, glass_color, glass_brilliance, glass_reflectivity, 0.35));
    hat.move(Point(-inner_width - front_side_top_length * cos(front_side_angle), 0, 0));
    objects.push_back(new VisiblePolygon(hat, glass_color, glass_brilliance, glass_reflectivity, 0.35));

    Polygon ground(std::vector<Point2D>({Point2D(0,0), Point2D(front_side_bottom_length, 0), Point2D(front_side_bottom_length, lateral_side_bottom_length), Point2D(0, lateral_side_bottom_length)}));
    ground.rotate_x(-M_PI_2);
    ground.move(spawn_point + Point(inner_width / 2, 0, 0));
    objects.push_back(new VisiblePolygon(ground, 0xffaaaaaa));
    ground.move(Point(-inner_width - front_side_top_length * cos(front_side_angle), 0, 0));
    objects.push_back(new VisiblePolygon(ground, 0xffaaaaaa));

}

VisibleLibraryEntrance::~VisibleLibraryEntrance()
{
    destroy();
}
