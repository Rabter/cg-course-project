#include <cmath>
#include <cassert>
#include "visible_sphere.h"

VisibleSphere::VisibleSphere(const Point &center, double radius, const Color &color, double brilliance, double reflectivity, double transperancy):
    brilliance(brilliance), reflectivity(reflectivity), transperancy(transperancy), tester(center, radius), center(center), radius(radius), color(color) {}

bool VisibleSphere::find(double &intersection_t, Color &color, Point &normal, double &brilliance, double &reflectivity, double &transperancy, const Ray &ray, double t_min) const
{
    if (!test(ray))
        return false;
    Point ray_first = ray.direction().first(), dir = ray.direction().RVector();
    Point OC = ray_first - center;
    double a = dir * dir;
    double b = OC * dir * 2;
    double c = OC * OC - radius * radius;
    double discriminant = b * b - 4 * a * c;
    if (discriminant < 0) // On some resolutions some pixels may be visible by test() but invisible by find(). Proboubly becouse of calculation inaccuracy
        return false;
    discriminant = sqrt(discriminant);
    double t = (-b - discriminant) / (2 * a);
    if (t < t_min)
    {
        t = (-b + discriminant) / (2 * a);
        if (t < t_min)
            return false;
    }
    intersection_t = t;
    color = this->color;
    brilliance = this->brilliance;
    reflectivity = this->reflectivity;
    transperancy = this->transperancy;
    normal = (ray_first + dir * t) - center;
    normal = normal / normal.length();
    if (normal * ray.direction().RVector() > 0)
        normal = normal * -1;
    return true;
}

bool VisibleSphere::has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const
{
    if (!test(ray))
        return false;
    Point ray_first = ray.direction().first(), dir = ray.direction().RVector();
    Point OC = ray_first - center;
    double a = dir * dir;
    double b = OC * dir * 2;
    double c = OC * OC - radius * radius;
    double discriminant = b * b - 4 * a * c;
    if (discriminant < 0) // On some resolutions some pixels may be visible by test() but invisible by find(). Proboubly becouse of calculation inaccuracy
        return false;
    bool found = false;
    transperancy = 1;
    discriminant = sqrt(discriminant);
    double intersection_t = (-b - discriminant) / (2 * a);
    if (intersection_t >= t_min && intersection_t <= t_max)
    {
        transperancy *= this->transperancy;
        found = true;
    }

    intersection_t = (-b + discriminant) / (2 * a);
    if (intersection_t >= t_min && intersection_t <= t_max)
    {
        transperancy *= this->transperancy;
        found = true;
    }

    return found;
}

bool VisibleSphere::test(const Ray &ray) const
{
    return tester.test(ray.direction());
}
