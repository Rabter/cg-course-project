#ifndef VISIBLERHOMBICUBOCTAHEDRON_H
#define VISIBLERHOMBICUBOCTAHEDRON_H

#include "visible_polygon.h"
#include "geometry/geometric_objects.h"
#include "geometry/ray_trace_testers.h"

class VisibleRhombicuboctahedron : public VisibleObject
{
public:
    VisibleRhombicuboctahedron(const Rhombicuboctahedron &shape, const Color &color, double brilliance = -1, double reflectivity = -1, double transperancy = 0);
    ~VisibleRhombicuboctahedron();

    virtual bool find(double &intersection_t, Color &color, Point &normal, double &brilliance, double &reflectivity, double &transperancy, const Ray &ray, double t_min) const override;
    virtual bool has_intersection(double &transperancy, const Ray &ray, double t_min, double t_max) const override;
    virtual bool test(const Ray &ray) const override;

private:
    VisiblePolygon *visible_parts[26];
    Rhombicuboctahedron shape;
    Color color;
    SphereRayTester *tester;
};

#endif // VISIBLERHOMBICUBOCTAHEDRON_H
