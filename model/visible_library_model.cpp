#include <cmath>
#include "visible_library_model.h"
#include "visible_rhombicuboctahedron.h"
#include "visible_pipe.h"
#include "visible_circle.h"
#include "visible_library_entrails.h"
#include "visible_library_entrance.h"
#include "glass.h"

VisibleLibraryModel::VisibleLibraryModel(Point spawn_point): base1_shift(0, 0, 0), base2_shift(Point(0,base1_height,0) + base1_shift), base3_shift(Point(0,base2_height,0) + base2_shift),
    head_shift(Point(0,edge_dist + base3_height,0) + base3_shift), head_center(spawn_point + head_shift)
{
    objects.push_back(new VisibleRhombicuboctahedron(Rhombicuboctahedron(head_center, head_side_length), glass_color, glass_brilliance, glass_reflectivity, glass_transperancy));
    objects.push_back(new VisibleRhombicuboctahedron(Rhombicuboctahedron(head_center, head_side_length * 0.95), 0xffffffff, 1));

    constexpr double prot_half_width = head_side_length / 8, prot_top_dist = edge_dist  * 3 / 4, prot_depth = 115;
    constexpr double main_tower_radius = head_side_halflength / 2, side_tower_radius = main_tower_radius / 3;
    Point main_tower_base1(spawn_point + Point(0,0,head_center.z() - head_side_halflength - prot_depth)), main_tower_base2(spawn_point + Point(0,base1_height + base2_height + base3_height + 2 * edge_dist,head_center.z() - head_side_halflength - prot_depth));
    Point side_tower_base1(main_tower_base1 + Point(main_tower_radius + side_tower_radius, 0, 0)), side_tower_base2(main_tower_base2 + Point(main_tower_radius + side_tower_radius, 0, 0));
    Rectangle tower_protrusion1(Point(head_center.x() - prot_half_width, head_center.y() - edge_dist, head_center.z() - head_side_halflength),
                                Point(head_center.x() - prot_half_width, head_center.y() + prot_top_dist, head_center.z() - head_side_halflength - prot_depth));
    Rectangle tower_protrusion2(Point(head_center.x() + prot_half_width, head_center.y() - edge_dist, head_center.z() - head_side_halflength),
                                Point(head_center.x() + prot_half_width, head_center.y() + prot_top_dist, head_center.z() - head_side_halflength - prot_depth));
    Rectangle tower_connector1(side_tower_base1 + Point(0,0,-side_tower_radius), side_tower_base2 + Point(-2*side_tower_radius,0,-side_tower_radius));
    Rectangle tower_connector2(tower_connector1);
    tower_connector2.move(Point(0,0, 2*side_tower_radius));
    Rectangle tower_protrusion3(Point(2 * prot_half_width, prot_depth, 0));
    Rectangle tower_connector3(Point(-2 * side_tower_radius, 2 * side_tower_radius, 0));
    tower_protrusion3.rotate_x(M_PI_2);
    tower_protrusion3.move(spawn_point + Point(head_center.x() - prot_half_width, head_center.y() + prot_top_dist, head_center.z() - head_side_halflength));
    tower_connector3.rotate_x(M_PI_2);
    tower_connector3.move(side_tower_base2 - spawn_point + Point(0, 0, side_tower_radius));
    objects.push_back(new VisiblePolygon(tower_protrusion1, glass_color, glass_brilliance, glass_reflectivity, glass_transperancy));
    objects.push_back(new VisiblePolygon(tower_protrusion2, glass_color, glass_brilliance, glass_reflectivity, glass_transperancy));
    objects.push_back(new VisiblePolygon(tower_protrusion3, glass_color, glass_brilliance, glass_reflectivity));
    objects.push_back(new VisiblePolygon(tower_connector1, glass_color, glass_brilliance, 0.5));
    objects.push_back(new VisiblePolygon(tower_connector2, glass_color, glass_brilliance, 0.5));
    objects.push_back(new VisiblePolygon(tower_connector3, glass_color, glass_brilliance, 0.5));

    objects.push_back(new VisiblePipe(Cylinder(main_tower_base1, main_tower_base2, main_tower_radius), glass_color, glass_brilliance, 0.5));
    objects.push_back(new VisibleCircle(Circle(main_tower_base2, main_tower_base2 - main_tower_base1, main_tower_radius), glass_color, glass_brilliance, 0.5));
    objects.push_back(new VisiblePipe(Cylinder(side_tower_base1, side_tower_base2, side_tower_radius), glass_color, glass_brilliance, 0.5));
    objects.push_back(new VisibleCircle(Circle(side_tower_base2, side_tower_base2 - side_tower_base1, side_tower_radius), glass_color, glass_brilliance, 0.5));

    objects.push_back(new VisiblePipe(Cylinder(spawn_point + base1_shift, spawn_point + base1_shift + Point(0,base1_height * mirror1_part,0), base1_radius), glass_color, glass_brilliance, 0.5, 0));
    objects.push_back(new VisiblePipe(Cylinder(spawn_point + base1_shift + Point(0,base1_height * mirror1_part,0), spawn_point + base1_shift + Point(0,base1_height,0), base1_radius), 0xffffffff));
    objects.push_back(new VisibleCircle(Circle(spawn_point + base1_shift, Point(0,1,0), base1_radius), 0xff888888));
    objects.push_back(new VisibleCircle(Circle(spawn_point + base1_shift + Point(0,base1_height,0), Point(0,1,0), base1_radius), 0xff888888));

    objects.push_back(new VisiblePipe(Cylinder(spawn_point + base2_shift, spawn_point + base2_shift + Point(0,base2_height * mirror2_part,0), base2_radius), glass_color, glass_brilliance, 0.5, 0));
    objects.push_back(new VisiblePipe(Cylinder(spawn_point + base2_shift + Point(0,base2_height * mirror2_part,0), spawn_point + base2_shift + Point(0,base2_height,0), base2_radius), 0xffffffff));
    objects.push_back(new VisibleCircle(Circle(spawn_point + base2_shift + Point(0,base2_height,0), Point(0,1,0), base2_radius), 0xff888888));

    objects.push_back(new VisiblePipe(Cylinder(spawn_point + base3_shift, spawn_point + base3_shift + Point(0,base3_height * mirror3_part,0), base3_radius), glass_color, glass_brilliance, 0.5, 0));
    objects.push_back(new VisiblePipe(Cylinder(spawn_point + base3_shift + Point(0,base3_height * mirror3_part,0), spawn_point + base3_shift + Point(0,base3_height,0), base3_radius), 0xffffffff));
    objects.push_back(new VisibleCircle(Circle(spawn_point + base3_shift + Point(0,base3_height,0), Point(0,1,0), base3_radius), 0xff888888));

    objects.push_back(new VisibleLibraryEntrails(head_center, head_side_length * 0.952));

    objects.push_back(new VisibleLibraryEntrance(spawn_point + Point(0,0,150)));
}

VisibleLibraryModel::~VisibleLibraryModel()
{
    destroy();
}
