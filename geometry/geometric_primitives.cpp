#include <cmath>
#include "geometric_primitives.h"
#include "flat_zero_rotation.h"

Point operator - (const Point &point)
{
    return Point(-point.x(), -point.y(), -point.z());
}

Point operator - (const Point &first, const Point &second)
{
    return Point(first.x() - second.x(), first.y() - second.y(), first.z() - second.z());
}

Point operator + (const Point &first, const Point &second)
{
    return Point(first.x() + second.x(), first.y() + second.y(), first.z() + second.z());
}

Point operator * (const Point &point, double mul)
{
    return Point(point.x() * mul, point.y() * mul, point.z() * mul);
}

Point operator / (const Point &point, double div)
{
    return Point(point.x() / div, point.y() / div, point.z() / div);
}

double Point::length() const
{
    return sqrt(_x * _x + _y * _y + _z * _z);
}

void Point::rotate_x(double angle)
{
    flat_zero_rotation(_y, _z, angle);
}

void Point::rotate_y(double angle)
{
    flat_zero_rotation(_x, _z, angle);
}

void Point::rotate_z(double angle)
{
    flat_zero_rotation(_x, _y, angle);
}

Point Point::operator +=(const Point &other)
{
    _x += other._x;
    _y += other._y;
    _z += other._z;
    return *this;
}

Point Point::operator -=(const Point &other)
{
    _x -= other._x;
    _y -= other._y;
    _z -= other._z;
    return *this;
}

Point Point::operator /= (double divider)
{
    _x /= divider;
    _y /= divider;
    _z /= divider;
    return *this;
}

double operator * (const Point &p1, const Point &p2)
{
    return p1._x * p2._x + p1._y * p2._y + p1._z * p2._z;
}

Point operator ^ (const Point &p1, const Point &p2)
{
    double x = p1._y * p2._z - p1._z * p2._y;
    double y = p1._z * p2._x - p1._x * p2._z;
    double z = p1._x * p2._y - p1._y * p2._x;
    return Point(x, y, z);
}

