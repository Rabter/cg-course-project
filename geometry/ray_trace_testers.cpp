#include <cmath>
#include "ray_trace_testers.h"
#include "micro_math.h"

SphereRayTester::SphereRayTester(const Point &center, double radius)
{
    this->center = center;
    this->radius = radius;
}

bool SphereRayTester::test(const Line &ray) const
{
    double a = ray.RVector().x(), b = ray.RVector().y(), c = ray.RVector().z();
    Point rp = ray.first();
    double t = -(a * (rp.x() - center.x()) + b * (rp.y() - center.y()) + c * (rp.z() - center.z())) / (a * a + b * b + c * c);
    if (sqr(rp.x() + a * t - center.x()) + sqr(rp.y() + b * t - center.y()) + sqr(rp.z() + c * t - center.z()) > sqr(radius))
        return false;
    return true;
}

RectRayTester::RectRayTester(Parallelepiped bounder): bounder(bounder) {}

bool RectRayTester::test(const Line &ray) const
{
    Point shift = -ray.first();
    Point second = ray.second() + shift;
    std::vector<Point> bounder_points;
    double xmin, xmax, ymin, ymax;
    double z_angle, y_angle, x_angle = -atan(second.y() / second.z());
    double z = second.z() * cos(x_angle) - second.y() * sin(x_angle);
    Parallelepiped current_bounder(bounder);
    y_angle = -atan(second.x() / z);
    current_bounder.move(shift);
    current_bounder.rotate_x(x_angle);
    current_bounder.rotate_y(y_angle);

    bounder_points = current_bounder.get_points();
    double dxmax = bounder_points[7].x() - bounder_points[0].x(), dy = bounder_points[7].y() - bounder_points[0].y();
    double adxmax = templ_abs<double>(dxmax);
    for (unsigned i = 0; i < 7; ++i)
    {
        double tmp = bounder_points[i + 1].x() - bounder_points[i].x();
        double atmp = templ_abs<double>(tmp);
        if (atmp > adxmax)
        {
            dxmax = tmp;
            adxmax = atmp;
            dy = bounder_points[i + 1].y() - bounder_points[i].y();
        }
    }
    z_angle = -atan(dy / dxmax);
    current_bounder.rotate_z(z_angle);
    bounder_points = current_bounder.get_points();
    xmin = xmax = bounder_points[0].x();
    ymin = ymax = bounder_points[0].y();
    for (int i = 1; i < 8; ++i)
    {
        double x = bounder_points[i].x(), y = bounder_points[i].y();
        if (x < xmin)
            xmin = x;
        if (x > xmax)
            xmax = x;
        if (y < ymin)
            ymin = y;
        if (y > ymax)
            ymax = y;
    }
    return (((xmin <= 0 && xmax >= 0) || (xmin >= 0 && xmax <= 0)) && ((ymin <= 0 && ymax >= 0) || (ymin >= 0 && ymax <= 0)));
}
