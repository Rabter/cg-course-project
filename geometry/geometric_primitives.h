#ifndef GEOMETRIC_PRIMITIVES_H
#define GEOMETRIC_PRIMITIVES_H

#include <cmath>
#include "micro_math.h"

class Point2D
{
public:
    Point2D() = default;
    inline Point2D(double x, double y): _x(x), _y(y) {}

    inline void set(double x, double y)
    {
        _x = x;
        _y = y;
    }

    inline void set_x(double x) { _x = x; }
    inline void set_y(double y) { _y = y; }

    inline double x() const { return _x; }
    inline double y() const { return _y; }
protected:
    double _x, _y;
};

class Point // Can also be used as a vector
{
public:
    Point() = default;
    inline Point(double x, double y, double z)
    {
        _x = x;
        _y = y;
        _z = z;
    }

    inline Point(const Point2D &point_on_XOY)
    {
        _x = point_on_XOY.x();
        _y = point_on_XOY.y();
        _z = 0;
    }

    inline void set(double x, double y, double z)
    {
        _x = x;
        _y = y;
        _z = z;
    }

    inline void set_x(double x) { _x = x; }
    inline void set_y(double y) { _y = y; }
    inline void set_z(double z) { _z = z; }

    inline double x() const { return _x; }
    inline double y() const { return _y; }
    inline double z() const { return _z; }

    double length() const;

    void rotate_x(double angle);
    void rotate_y(double angle);
    void rotate_z(double angle);

    //   Radius-vector methods   //
    Point operator += (const Point &other);
    Point operator -= (const Point &other);
    Point operator /= (double divider);

    friend Point operator - (const Point &point);
    friend Point operator - (const Point &first, const Point &second);
    friend Point operator + (const Point &first, const Point &second);
    friend Point operator * (const Point &point, double mul);
    inline friend Point operator * (double mul, const Point &point) { return point * mul; }
    friend Point operator / (const Point &point, double div);
    //       Vector methods     //
    friend double operator * (const Point &p1, const Point &p2);
    friend Point operator ^ (const Point &p1, const Point &p2);
    //                          //

private:
    double _x, _y, _z;
};

class Line
{
public:
    inline Line(const Point &first, const Point &second): point(first), dir(second - first)
    { dir /= dir.length(); }

    inline const Point& first() const { return point; }
    inline Point second() const { return point + dir; }
    inline const Point& RVector() const { return dir; }

private:
    Point point, dir;
};

#endif // GEOMETRIC_PRIMITIVES_H
