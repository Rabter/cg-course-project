#ifndef RAY_TRACE_TESTERS_H
#define RAY_TRACE_TESTERS_H

#include "geometric_objects.h"

class RayTraceTestShape
{
public:
    RayTraceTestShape() = default;
    virtual ~RayTraceTestShape() = default;
    virtual bool test(const Line &ray) const = 0;
};

class SphereRayTester: public RayTraceTestShape
{
public:
    SphereRayTester(const Point &center, double radius);
    virtual ~SphereRayTester() = default;

    bool test(const Line &ray) const override;

private:
    Point center;
    double radius;
};

class RectRayTester: public RayTraceTestShape
{
public:
    RectRayTester(Parallelepiped bounder);
    virtual ~RectRayTester() = default;

    bool test(const Line &ray) const override;

private:
    Parallelepiped bounder;
};

#endif // RAY_TRACE_TESTERS_H
