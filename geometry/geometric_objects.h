#ifndef GEOMETRIC_OBJECTS_H
#define GEOMETRIC_OBJECTS_H

#include <vector>
#include "geometric_primitives.h"

class GeometricObject
{
public:
    virtual void move(const Point &shift) = 0;
    virtual void rotate_x(double angle) = 0;
    virtual void rotate_y(double angle) = 0;
    virtual void rotate_z(double angle) = 0;
};

class Polygon2D
{
public:
    // creates a polygon in OXY
    Polygon2D(const std::vector<Point2D> &points_on_XOY);
    Polygon2D(const Polygon2D &other);

    inline unsigned size() const
    { return points.size(); }

    inline std::vector<Point2D> get_points() const
    { return points; }

    inline const Point2D get_point(unsigned index) const
    { return points[index]; }

private:
    std::vector<Point2D> points;
};

class Parallelepiped: public GeometricObject
{
public:
    Parallelepiped(const Point &p1, const Point &p2 = Point(0, 0, 0));
    Parallelepiped(const Parallelepiped &other);

    virtual void move(const Point &shift) override;
    virtual void rotate_x(double angle) override;
    virtual void rotate_y(double angle) override;
    virtual void rotate_z(double angle) override;

    inline std::vector<Point> get_points() const
    { return points; }

    inline const Point get_point(unsigned index) const
    { return points[index]; }

private:
    std::vector<Point> points;
};

class Polygon: public GeometricObject
{
public:
    // creates a polygon in OXY
    Polygon(const std::vector<Point2D> &points_on_XOY);
    Polygon(const Polygon2D &polygon2d);
    Polygon(const Polygon &other);
    virtual ~Polygon() = default;

    virtual void move(const Point &shift) override;
    virtual void rotate_x(double angle) override;
    virtual void rotate_y(double angle) override;
    virtual void rotate_z(double angle) override;

    virtual Polygon2D to_polygon_2d(double *angle_x = nullptr, double *angle_y = nullptr, double *xy_dist = nullptr) const;

    inline unsigned size() const
    { return points.size(); }

    inline std::vector<Point> get_points() const
    { return points; }

    inline const Point get_point(unsigned index) const
    { return points[index]; }

protected:
    std::vector<Point> points;
    Polygon() = default;
};

class Rectangle: public Polygon
{
public:
    // creates a rectangle parallel to OY
    Rectangle(const Point &p1, const Point &p2 = Point(0, 0, 0));
    Rectangle(const Rectangle &other);
    virtual ~Rectangle() = default;

    virtual void move(const Point &shift) override;
    virtual void rotate_x(double angle) override;
    virtual void rotate_y(double angle) override;
    virtual void rotate_z(double angle) override;

};

class RectPyramid: public GeometricObject
{
public:
    RectPyramid(const Point &top, double x_width, double y_width);

    virtual void move(const Point &shift) override;
    virtual void rotate_x(double angle) override;
    virtual void rotate_y(double angle) override;
    virtual void rotate_z(double angle) override;

    inline Point top() const
    { return _top; }
    inline Rectangle base() const
    { return _rect; }
private:
    Rectangle _rect;
    Point _top;
};

class Rhombicuboctahedron: public GeometricObject
{
public:
    Rhombicuboctahedron(const Point &center, double side_length);
    ~Rhombicuboctahedron();

    inline Polygon get_side(unsigned i) const
    { return *sides[i]; }
    inline Point center() const
    { return _center; }

    virtual void move(const Point &shift) override;
    virtual void rotate_x(double angle) override;
    virtual void rotate_y(double angle) override;
    virtual void rotate_z(double angle) override;

private:
    Polygon *sides[26];
    Point _center;
};

class Cylinder: public GeometricObject
{
public:
    Cylinder(Point base1, Point base2, double radius);

    inline Point base1() const
    { return _base1; }
    inline Point base2() const
    { return _base2; }
    inline double radius() const
    { return _radius; }

    virtual void move(const Point &shift) override;
    virtual void rotate_x(double angle) override;
    virtual void rotate_y(double angle) override;
    virtual void rotate_z(double angle) override;

protected:
    Point _base1, _base2;
    double _radius;
};

class Circle: public GeometricObject
{
public:
    Circle(const Point2D point_on_xy, double radius);
    Circle(const Point &center, const Point &normal, double radius);

    inline Point center() const
    { return _center; }

    inline double radius() const
    { return _radius; }

    inline void plane_points(Point &a, Point &b, Point &c) const
    {
        a = _center;
        b = ring1;
        c = ring2;
    }

    inline Point normal() const
    { return (ring1 - _center) ^ (ring2 - _center); }

    virtual void move(const Point &shift) override;
    virtual void rotate_x(double angle) override;
    virtual void rotate_y(double angle) override;
    virtual void rotate_z(double angle) override;

protected:
    Point _center, ring1, ring2;
    double _radius;
};

#endif // GEOMETRIC_OBJECTS_H
