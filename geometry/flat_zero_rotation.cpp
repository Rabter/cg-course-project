#include <cmath>
#include "flat_zero_rotation.h"

void flat_zero_rotation(double &x, double &y, double angle)
{
    double tmp_x = x;
    x = x * cos(angle) + y * sin(angle);
    y = y * cos(angle) - tmp_x * sin(angle);
}
