#include <cmath>
#include "geometric_objects.h"
#include "flat_zero_rotation.h"
#include "micro_math.h"

Parallelepiped::Parallelepiped(const Point &p1, const Point &p2)
{
    points.resize(8);
    points[0] = p1;
    points[1] = Point(p1.x(), p1.y(), p2.z());
    points[2] = Point(p1.x(), p2.y(), p2.z());
    points[3] = Point(p1.x(), p2.y(), p1.z());
    points[4] = Point(p2.x(), p2.y(), p1.z());
    points[5] = Point(p2.x(), p1.y(), p1.z());
    points[6] = Point(p2.x(), p1.y(), p2.z());
    points[7]  = p2;
}

Parallelepiped::Parallelepiped(const Parallelepiped &other)
{
    points = other.points;
}

void Parallelepiped::move(const Point &shift)
{
    for (short i = 0; i < 8; ++i)
        points[i] += shift;
}

void Parallelepiped::rotate_x(double angle)
{
    for (Point &point : points)
        point.rotate_x(angle);
}

void Parallelepiped::rotate_y(double angle)
{
    for (Point &point : points)
        point.rotate_y(angle);
}
void Parallelepiped::rotate_z(double angle)
{
    for (Point &point : points)
        point.rotate_z(angle);
}

// Creates a rectangle parallel to OY
Rectangle::Rectangle(const Point &p1, const Point &p2)
{
    points.resize(4);
    points[2] = Point(p2.x(), p1.y(), p2.z());
    points[1] = p2;
    points[0] = Point(p1.x(), p2.y(), p1.z());
    points[3]  = p1;
}


Rectangle::Rectangle(const Rectangle &other): Polygon(other)
{
    points = other.points;
}

void Rectangle::move(const Point &shift)
{
    for (short i = 0; i < 4; ++i)
        points[i] += shift;
}

void Rectangle::rotate_x(double angle)
{
    for (Point &point : points)
        point.rotate_x(angle);
}

void Rectangle::rotate_y(double angle)
{
    for (Point &point : points)
        point.rotate_y(angle);
}
void Rectangle::rotate_z(double angle)
{
    for (Point &point : points)
        point.rotate_z(angle);
}

RectPyramid::RectPyramid(const Point &top, double x_width, double y_width):
    _rect(Point(-x_width / 2 + top.x(), -y_width / 2 + top.y(), 0), Point(x_width / 2 + top.x(), y_width / 2 + top.y(), 0)), _top(top) {}

void RectPyramid::move(const Point &shift)
{
    _top += shift;
    _rect.move(shift);
}

void RectPyramid::rotate_x(double angle)
{
    _top.rotate_x(angle);
    _rect.rotate_x(angle);
}

void RectPyramid::rotate_y(double angle)
{
    _top.rotate_y(angle);
    _rect.rotate_y(angle);
}

void RectPyramid::rotate_z(double angle)
{
    _top.rotate_z(angle);
    _rect.rotate_z(angle);
}

Polygon::Polygon(const std::vector<Point2D> &points_on_XOY): points(0)
{
    for (const Point2D &point: points_on_XOY)
        points.push_back(Point(point));
}

Polygon::Polygon(const Polygon2D &polygon2d): Polygon(polygon2d.get_points())
{}

Polygon::Polygon(const Polygon &other): points(other.points) {}

void Polygon::move(const Point &shift)
{
    for (Point &point: points)
        point += shift;
}

void Polygon::rotate_x(double angle)
{
    for (Point &point: points)
        point.rotate_x(angle);
}

void Polygon::rotate_y(double angle)
{
    for (Point &point: points)
        point.rotate_y(angle);
}

void Polygon::rotate_z(double angle)
{
    for (Point &point: points)
        point.rotate_z(angle);
}

#define EPS 1e-10
Polygon2D Polygon::to_polygon_2d(double *angle_x, double *angle_y, double *xy_dist) const
{
    Point tmp_vector = (points[0] - points[1]) ^ (points[2] - points[1]);
    Polygon res3d(*this);
    double angle = -(templ_abs<double>(tmp_vector.z()) < EPS? M_PI_2 : atan(tmp_vector.y() / tmp_vector.z())); // x angle
    res3d.rotate_x(angle);
    if (angle_x)
        *angle_x = angle;
    tmp_vector = (res3d.get_point(0) - res3d.get_point(1)) ^ (res3d.get_point(2) - res3d.get_point(1));
    angle = -(templ_abs<double>(tmp_vector.z()) < EPS? M_PI_2 : atan(tmp_vector.x() / tmp_vector.z())); // y angle
    res3d.rotate_y(angle);
    if (angle_y)
        *angle_y = angle;
    if (xy_dist)
        *xy_dist = res3d.get_point(0).z();
    std::vector<Point2D> res_points;
    for (unsigned i = 0; i < res3d.size(); ++i)
    {
        Point point = res3d.get_point(i);
        res_points.push_back(Point2D(point.x(), point.y()));
    }
    return Polygon2D(res_points);
}
#undef EPS

Polygon2D::Polygon2D(const std::vector<Point2D> &points_on_XOY)
{
    points = points_on_XOY;
}

Polygon2D::Polygon2D(const Polygon2D &other)
{
    this->points = other.points;
}


Rhombicuboctahedron::Rhombicuboctahedron(const Point &center, double side_length): _center(center)
{
    double halfside = side_length / 2;
    double edge_dist = halfside + side_length / sqrt(2);

    sides[0] = new Rectangle(Point(-edge_dist, -halfside, halfside) + center, Point(-edge_dist, halfside, -halfside) + center);
    sides[1] = new Rectangle(Point(-edge_dist, -halfside, -halfside) + center, Point(-halfside, halfside, -edge_dist) + center);
    sides[2] = new Rectangle(Point(-halfside, -halfside, -edge_dist) + center, Point(halfside, halfside, -edge_dist) + center);
    sides[3] = new Rectangle(Point(halfside, -halfside, -edge_dist) + center, Point(edge_dist, halfside, -halfside) + center);
    sides[4] = new Rectangle(Point(edge_dist, -halfside, -halfside) + center, Point(edge_dist, halfside, halfside) + center);
    sides[5] = new Rectangle(Point(edge_dist, -halfside, halfside) + center, Point(halfside, halfside, edge_dist) + center);
    sides[6] = new Rectangle(Point(halfside, -halfside, edge_dist) + center, Point(-halfside, halfside, edge_dist) + center);
    sides[7] = new Rectangle(Point(-halfside, -halfside, edge_dist) + center, Point(-edge_dist, halfside, halfside) + center);

    double sloped_square_coordinate = halfside + halfside / sqrt(2);
    sides[8] = new Rectangle(Point(0, -halfside, -halfside), Point(0, halfside, halfside));
    sides[8]->rotate_z(M_PI_4);
    sides[8]->move(Point(-sloped_square_coordinate, sloped_square_coordinate, 0) + center);
    sides[9] = new Rectangle(Point(-halfside, -halfside, 0), Point(halfside, halfside, 0));
    sides[9]->rotate_x(M_PI_4);
    sides[9]->move(Point(0, sloped_square_coordinate, sloped_square_coordinate) + center);
    sides[10] = new Rectangle(Point(0, -halfside, -halfside), Point(0, halfside, halfside));
    sides[10]->rotate_z(-M_PI_4);
    sides[10]->move(Point(sloped_square_coordinate, sloped_square_coordinate, 0) + center);
    sides[11] = new Rectangle(Point(-halfside, -halfside, 0), Point(halfside, halfside, 0));
    sides[11]->rotate_x(-M_PI_4);
    sides[11]->move(Point(0, sloped_square_coordinate, -sloped_square_coordinate) + center);
    sides[12] = new Rectangle(Point(0, -halfside, -halfside), Point(0, halfside, halfside));
    sides[12]->rotate_z(-M_PI_4);
    sides[12]->move(Point(-sloped_square_coordinate, -sloped_square_coordinate, 0) + center);
    sides[13] = new Rectangle(Point(-halfside, -halfside, 0), Point(halfside, halfside, 0));
    sides[13]->rotate_x(-M_PI_4);
    sides[13]->move(Point(0, -sloped_square_coordinate, sloped_square_coordinate) + center);
    sides[14] = new Rectangle(Point(0, -halfside, -halfside), Point(0, halfside, halfside));
    sides[14]->rotate_z(M_PI_4);
    sides[14]->move(Point(sloped_square_coordinate, -sloped_square_coordinate, 0) + center);
    sides[15] = new Rectangle(Point(-halfside, -halfside, 0), Point(halfside, halfside, 0));
    sides[15]->rotate_x(M_PI_4);
    sides[15]->move(Point(0, -sloped_square_coordinate, -sloped_square_coordinate) + center);

    sides[16] = new Rectangle(Point(-halfside, -halfside, 0), Point(halfside, halfside, 0));
    sides[16]->rotate_x(M_PI_2);
    sides[16]->move(Point(0,-edge_dist,0) + center);
    sides[17] = new Rectangle(Point(-halfside, -halfside, 0), Point(halfside, halfside, 0));
    sides[17]->rotate_x(M_PI_2);
    sides[17]->move(Point(0,edge_dist,0) + center);

    const double triangle_angle = M_PI_2 - acos(1 / sqrt(3));
    std::vector<Point2D> shaper = { Point2D(0,0), Point2D(side_length, 0), Point2D(halfside, halfside * sqrt(3)) };
    sides[18] = new Polygon(shaper);
    sides[19] = new Polygon(*sides[18]);
    sides[20] = new Polygon(*sides[18]);
    sides[21] = new Polygon(*sides[18]);

    sides[18]->rotate_x(-triangle_angle);
    sides[18]->rotate_y(M_PI_4);
    sides[18]->move(Point(-edge_dist, halfside, -halfside) + center);
    sides[19]->rotate_x(triangle_angle);
    sides[19]->rotate_y(-M_PI_4);
    sides[19]->move(Point(-edge_dist, halfside, halfside) + center);
    sides[20]->rotate_x(triangle_angle);
    sides[20]->rotate_y(M_PI_4);
    sides[20]->move(Point(halfside, halfside, edge_dist) + center);
    sides[21]->rotate_x(-triangle_angle);
    sides[21]->rotate_y(-M_PI_4);
    sides[21]->move(Point(halfside, halfside, -edge_dist) + center);

    shaper = { Point2D(0,0), Point2D(side_length, 0), Point2D(halfside, -halfside * sqrt(3)) };
    sides[22] = new Polygon(shaper);
    sides[23] = new Polygon(*sides[22]);
    sides[24] = new Polygon(*sides[22]);
    sides[25] = new Polygon(*sides[22]);

    sides[22]->rotate_x(triangle_angle);
    sides[22]->rotate_y(M_PI_4);
    sides[22]->move(Point(-edge_dist, -halfside, -halfside) + center);
    sides[23]->rotate_x(-triangle_angle);
    sides[23]->rotate_y(-M_PI_4);
    sides[23]->move(Point(-edge_dist, -halfside, halfside) + center);
    sides[24]->rotate_x(-triangle_angle);
    sides[24]->rotate_y(M_PI_4);
    sides[24]->move(Point(halfside, -halfside, edge_dist) + center);
    sides[25]->rotate_x(triangle_angle);
    sides[25]->rotate_y(-M_PI_4);
    sides[25]->move(Point(halfside, -halfside, -edge_dist) + center);
}

Rhombicuboctahedron::~Rhombicuboctahedron()
{
    for (unsigned i = 0; i < 26; ++i)
        delete sides[i];
}

void Rhombicuboctahedron::move(const Point &shift)
{
    _center += shift;
    for (Polygon *&side : sides)
        side->move(shift);
}

void Rhombicuboctahedron::rotate_x(double angle)
{
    _center.rotate_x(angle);
    for (Polygon *&side : sides)
        side->rotate_x(angle);
}

void Rhombicuboctahedron::rotate_y(double angle)
{
    _center.rotate_y(angle);
    for (Polygon *&side : sides)
        side->rotate_y(angle);
}

void Rhombicuboctahedron::rotate_z(double angle)
{
    _center.rotate_z(angle);
    for (Polygon *&side : sides)
        side->rotate_z(angle);
}

Cylinder::Cylinder(Point base1, Point base2, double radius): _base1(base1), _base2(base2), _radius(radius) {}

void Cylinder::move(const Point &shift)
{
    _base1 += shift;
    _base2 += shift;
}

void Cylinder::rotate_x(double angle)
{
    _base1.rotate_x(angle);
    _base2.rotate_x(angle);
}

void Cylinder::rotate_y(double angle)
{
    _base1.rotate_y(angle);
    _base2.rotate_y(angle);
}

void Cylinder::rotate_z(double angle)
{
    _base1.rotate_z(angle);
    _base2.rotate_z(angle);
}

Circle::Circle(const Point2D point_on_xy, double radius):
    _center(point_on_xy), ring1(Point(_center.x() + radius, _center.y(), 0)), ring2(Point(_center.x(), _center.y() + radius, 0)), _radius(radius) {}

#define EPS 1e-10
Circle::Circle(const Point &center, const Point &normal, double radius): _center(center), _radius(radius)
{
    if (normal.x() < EPS)
    {
        ring1.set(1, 0, 0);
        ring1 = ring1 * _radius;
    }
    else
    {
        ring1.set_x(-(normal.y() + normal.z()) / normal.x());
        ring1.set_y(1);
        ring1.set_z(1);
        ring1 = ring1 * _radius / ring1.length();
    }
    ring2 = ring1 ^ normal;
    ring2 = ring2 * _radius / ring2.length();
    ring1 += _center;
    ring2 += _center;
}
#undef EPS

void Circle::move(const Point &shift)
{
    _center += shift;
    ring1 += shift;
    ring2 += shift;
}

void Circle::rotate_x(double angle)
{
    _center.rotate_x(angle);
    ring1.rotate_x(angle);
    ring2.rotate_x(angle);
}

void Circle::rotate_y(double angle)
{
    _center.rotate_y(angle);
    ring1.rotate_y(angle);
    ring2.rotate_y(angle);
}

void Circle::rotate_z(double angle)
{
    _center.rotate_z(angle);
    ring1.rotate_z(angle);
    ring2.rotate_z(angle);
}
