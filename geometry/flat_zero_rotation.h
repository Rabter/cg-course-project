#ifndef FLAT_ZERO_ROTATION_H
#define FLAT_ZERO_ROTATION_H

void flat_zero_rotation(double &x, double &y, double angle);

#endif // FLAT_ZERO_ROTATION_H
