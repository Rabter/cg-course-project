#ifndef CAMERA_H
#define CAMERA_H

#include "viewscreen.h"
#include "geometry/geometric_primitives.h"
#include "geometry/geometric_objects.h"
#include <scene/scene.h>

class Camera: public RectPyramid
{
public:
    Camera(Scene *scene, const RectPyramid &shape, const ViewScreen &screen);
    void render();
    inline ViewScreen result() const
    { return screen; }
private:
    ViewScreen screen;
    Scene *scene;
};

#endif // CAMERA_H
