#include "camera.h"

Camera::Camera(Scene *scene, const RectPyramid &shape, const ViewScreen &screen): RectPyramid(shape),
    screen(screen), scene(scene) {}

void Camera::render()
{
    Point topleft = base().get_point(0);
    Point inner_step((base().get_point(1) - topleft) / screen.width()), outer_step((base().get_point(3) - topleft) / screen.height());
    Point outer_runner = topleft;
    Point view_point = top();
    const double screen_max_distance = (topleft - view_point).length();
    for (unsigned height_iteration = 0; height_iteration < screen.height(); ++height_iteration)
    {
        Point inner_runner = outer_runner;
        for (unsigned width_iteration = 0; width_iteration < screen.width(); ++width_iteration)
        {
            Ray ray(Line(view_point, inner_runner));
            screen.at(width_iteration, height_iteration) = ray.run(*scene, screen_max_distance);
            inner_runner += inner_step;
        }
        outer_runner += outer_step;
    }
}
