#include "viewscreen.h"

ViewScreen::ViewScreen(unsigned width, unsigned height):
    colors(width * height), _width(width), _height(height) {}

void ViewScreen::reset(unsigned width, unsigned height)
{
    _width = width;
    _height = height;
    colors.resize(width * height);
}

ARGB &ViewScreen::at(unsigned i, unsigned j)
{
    return colors[i + j * _width];
}
