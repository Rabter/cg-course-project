#ifndef VIEWSCREEN_H
#define VIEWSCREEN_H

#include <vector>
#include "color/argb.h"

class ViewScreen
{
public:
    ViewScreen(unsigned width, unsigned height);
    void reset(unsigned width, unsigned height);

    inline unsigned width() const
    { return _width; }
    inline unsigned height() const
    { return _height; }

    ARGB& at(unsigned i, unsigned j);

    std::vector<ARGB> colors;

private:
    unsigned _width, _height;
};

#endif // VIEWSCREEN_H
