#-------------------------------------------------
#
# Project created by QtCreator 2019-06-30T15:59:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Project
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
    camera/camera.cpp \
    camera/viewscreen.cpp \
    color/color.cpp \
    doubleentry.cpp \
    geometry/geometric_objects.cpp \
    geometry/geometric_primitives.cpp \
    geometry/flat_zero_rotation.cpp \
    geometry/ray_trace_testers.cpp \
    light/ambient_light.cpp \
    light/direct_light.cpp \
    light/point_light.cpp \
    main.cpp \
    mainwindow.cpp \
    model/model.cpp \
    model/visible_circle.cpp \
    model/visible_library_entrails.cpp \
    model/visible_library_entrance.cpp \
    model/visible_library_model.cpp \
    model/visible_pipe.cpp \
    model/visible_polygon.cpp \
    model/visible_rhombicuboctahedron.cpp \
    model/visible_sphere.cpp \
    ray/ray.cpp \
    scene/scene.cpp \
    validentry.cpp

HEADERS += \
    camera/camera.h \
    camera/viewscreen.h \
    color/argb.h \
    color/color.h \
    doubleentry.h \
    geometry/geometric_objects.h \
    geometry/geometric_primitives.h \
    geometry/flat_zero_rotation.h \
    geometry/ray_trace_testers.h \
    light/ambient_light.h \
    light/direct_light.h \
    light/lights.h \
    light/point_light.h \
    mainwindow.h \
    micro_math.h \
    model/glass.h \
    model/model.h \
    model/visible_circle.h \
    model/visible_library_entrails.h \
    model/visible_library_entrance.h \
    model/visible_library_model.h \
    model/visible_object.h \
    model/visible_pipe.h \
    model/visible_polygon.h \
    model/visible_rhombicuboctahedron.h \
    model/visible_sphere.h \
    ray/ray.h \
    scene/scene.h \
    scene/base_scene.h \
    validentry.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
