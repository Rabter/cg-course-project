#include <QImage>
#include <QVector>
#include <QPixmap>
#include <cmath>
//#include <chrono>
//#include <QDebug>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "model/visible_sphere.h"
#include "model/visible_library_model.h"
#include "light/lights.h"

static constexpr double radian_coefficient = M_PI / 180;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), scene_center(0,80,0)
{
    ui->setupUi(this);
    QGraphicsScene *display = new QGraphicsScene(this);
    ui->graphicsView->setScene(display);
    display->setSceneRect(0,0, ui->graphicsView->size().width() - 5, ui->graphicsView->size().height() - 5);
    initialize_scene();
    ui->info_label->setText("Ready to render");
    connect(this, SIGNAL(result_is_obsolete()), this, SLOT(obsolete_handler()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_render_clicked()
{
    update_result();
}

void MainWindow::initialize_scene()
{
    VisibleSphere *ground = new VisibleSphere(Point(0, -100000, 0), 100000, Color(0xff55ff55), -1, -1);
    VisibleLibraryModel *model = new VisibleLibraryModel(Point(0,0,0));
    scene.set_background(0xff44a5ef);
    scene.add(ground);
    scene.add(model);
    scene.lights.ambient = AmbientLight::instance(0.2);
    Point source_point(0, 0, -ui->slider_light_point_zoom->value());
    scene.lights.point.push_back(new PointLight(source_point, ui->slider_light_intensity->value() / 100.0));
    light_horizontal_angle = 0;
    light_vertical_angle = 0;
    rotate_light_vertical(ui->slider_light_vertical->value());
    rotate_light_horizontal(-ui->dial_light_horizontal->value());
    RectPyramid pyr(Point(0,0,50), 50, 35);
    pyr.move(Point(0,50,700) + scene_center);
    camera = new Camera(&scene, pyr, ViewScreen(500, 350));
    camera_horizontal_angle = camera_vertical_angle = 0;
    rotate_camera_vertical(ui->slider_camera_vertical->value());
    rotate_camera_horizontal(-ui->dial_camera_horizontal->value());
}

void MainWindow::update_result()
{
    double intensity;
    if(ui->entry_ambient->get_double(intensity))
    {
        scene.lights.ambient->delete_instance();
        scene.lights.ambient = AmbientLight::instance(intensity);

        ui->info_label->setText("Rendering, please, wait...");
        ui->info_label->setStyleSheet("color: black");
        ui->info_label->repaint();

        ui->graphicsView->scene()->clear();
//        std::chrono::time_point<std::chrono::system_clock> tb, te;
//        tb = std::chrono::system_clock::now();
        camera->render();
//        te = std::chrono::system_clock::now();
//        qDebug() << static_cast<double>(std::chrono::duration_cast<std::chrono::nanoseconds>(te - tb).count());
        std::vector<ARGB> colors = camera->result().colors;
        unsigned char* data = reinterpret_cast<unsigned char*>(colors.data()); //ToDo: why doesn't dynamic cast work?
        QImage result(data, 500, 350, QImage::Format_ARGB32);
        ui->graphicsView->scene()->addPixmap(QPixmap::fromImage(result));
        set_info_done();
    }
}

void MainWindow::obsolete_handler()
{
    if (ui->cb_rerender->isChecked())
        update_result();
    else
    {
        if (ui->info_label->text() == "Finished rendering")
        {
            ui->info_label->setText("The image is obsolete, please, re-render");
            ui->info_label->setStyleSheet("color: orange");
        }
    }
}

void MainWindow::set_info_done()
{
    ui->info_label->setText("Finished rendering");
    ui->info_label->setStyleSheet("color: green");
}

void MainWindow::rotate_light_horizontal(int value)
{
    if (ui->rb_point_light->isChecked())
    {
        PointLight *point = scene.lights.point[0];
        point->move(-scene_center);
        point->rotate_y((value - light_horizontal_angle) * radian_coefficient);
        point->move(scene_center);
        light_horizontal_angle = value;
    }
    else
    {
        DirectLight *light = scene.lights.direct[0];
        light->rotate_y((value - light_horizontal_angle) * radian_coefficient);
        light_horizontal_angle = value;
    }

    emit result_is_obsolete();
}

void MainWindow::rotate_light_vertical(int value)
{
    if (ui->rb_point_light->isChecked())
    {
        PointLight *point = scene.lights.point[0];
        point->move(-scene_center);
        point->rotate_y(-light_horizontal_angle * radian_coefficient);
        point->rotate_x((value - light_vertical_angle) * radian_coefficient);
        point->rotate_y(light_horizontal_angle * radian_coefficient);
        point->move(scene_center);
        light_vertical_angle = value;
    }
    else
    {
        DirectLight *light = scene.lights.direct[0];
        light->rotate_y(-light_horizontal_angle * radian_coefficient);
        light->rotate_x((value - light_vertical_angle) * radian_coefficient);
        light->rotate_y(light_horizontal_angle * radian_coefficient);
        light_vertical_angle = value;
    }

    emit result_is_obsolete();
}

void MainWindow::rotate_camera_horizontal(int value)
{
    camera->move(-scene_center);
    camera->rotate_y((value - camera_horizontal_angle) * radian_coefficient);
    camera->move(scene_center);
    camera_horizontal_angle = value;

    emit result_is_obsolete();
}

void MainWindow::rotate_camera_vertical(int value)
{
    camera->move(-scene_center);
    camera->rotate_y(-camera_horizontal_angle * radian_coefficient);
    camera->rotate_x((value - camera_vertical_angle) * radian_coefficient);
    camera->rotate_y(camera_horizontal_angle * radian_coefficient);
    camera->move(scene_center);
    camera_vertical_angle = value;

    emit result_is_obsolete();
}


void MainWindow::on_dial_camera_horizontal_valueChanged(int value)
{
    rotate_camera_horizontal(-value);
}

void MainWindow::on_slider_camera_vertical_valueChanged(int value)
{
    rotate_camera_vertical(value);
}

void MainWindow::on_slider_camera_zoom_valueChanged(int value)
{
    Point cam_dist = camera->top() - scene_center;
    Point old_cam_dist = cam_dist;
    cam_dist /= cam_dist.length();
    cam_dist = cam_dist * -value;
    camera->move(cam_dist - old_cam_dist);

    emit result_is_obsolete();
}

void MainWindow::on_dial_light_horizontal_valueChanged(int value)
{
    rotate_light_horizontal(-value);
}

void MainWindow::on_slider_light_vertical_valueChanged(int value)
{
    rotate_light_vertical(value);
}

void MainWindow::on_rb_point_light_toggled(bool checked)
{
    if (checked)
    {
        ui->slider_light_point_zoom->setEnabled(true);
        Point source_point(0,0, -ui->slider_light_point_zoom->value());
        scene.lights.point.push_back(new PointLight(source_point, ui->slider_light_intensity->value() / 100.0));
        light_horizontal_angle = 0;
        light_vertical_angle = 0;
        rotate_light_vertical(ui->slider_light_vertical->value());
        rotate_light_horizontal(-ui->dial_light_horizontal->value());
    }
    else
    {
        delete scene.lights.point[0];
        scene.lights.point.clear();
    }

    emit result_is_obsolete();
}

void MainWindow::on_rb_direct_light_toggled(bool checked)
{
    if (checked)
    {
        ui->slider_light_point_zoom->setDisabled(true);
        Point direction(0, 0, -1);
        scene.lights.direct.push_back(new DirectLight(direction, ui->slider_light_intensity->value() / 100.0));
        light_horizontal_angle = 0;
        light_vertical_angle = 0;
        rotate_light_vertical(ui->slider_light_vertical->value());
        rotate_light_horizontal(-ui->dial_light_horizontal->value());
    }
    else
    {
        delete scene.lights.direct[0];
        scene.lights.direct.clear();
    }

    emit result_is_obsolete();
}

void MainWindow::on_slider_light_intensity_valueChanged(int value)
{
    if (ui->rb_point_light->isChecked())
        scene.lights.point[0]->set_intensity(value / 100.0);
    else
        scene.lights.direct[0]->set_intensity(value / 100.0);

    emit result_is_obsolete();
}

void MainWindow::on_slider_light_point_zoom_valueChanged(int value)
{
    if (ui->rb_point_light->isChecked())
    {
        PointLight *point = scene.lights.point[0];
        Point point_dist = point->source_point() - scene_center;
        Point old_point_dist = point_dist;
        point_dist /= point_dist.length();
        point_dist = point_dist * -value;
        point->move(point_dist - old_point_dist);

        emit result_is_obsolete();
    }
}
