#ifndef COLOR_H
#define COLOR_H

#include "argb.h"

class BaseColor
{
public:
    BaseColor(ARGB value = 0xff000000);

    virtual ARGB to_argb() = 0;

protected:
    unsigned char red, green, blue;
};

class Color: public BaseColor
{
public:
    Color(ARGB value = 0xff000000);

    ARGB to_argb();

    Color operator += (const Color &other);
    Color operator *= (double mul);

    friend Color operator + (const Color& c1, const Color &c2);
    friend Color operator * (const Color& color, double mul);

};

// Old color class. Not used in this project because opacity is always 255, objects'es opacities are counted outside color class
class TransparentColor: public BaseColor
{
public:
    TransparentColor(ARGB value = 0xff000000);

    ARGB to_argb();

    TransparentColor operator += (const TransparentColor &other);
    TransparentColor operator *= (double mul);

    friend TransparentColor operator + (const TransparentColor& c1, const TransparentColor &c2);
    friend TransparentColor operator * (const TransparentColor& color, double mul);

protected:
    unsigned char opacity;
};

#endif // COLOR_H
