#include "color.h"
#include "micro_math.h"

BaseColor::BaseColor(ARGB value)
{
    blue = value % 256;
    value >>= 8;
    green = value % 256;
    value >>= 8;
    red = value % 256; // % 256 is probuably not needed here but is safer
}

Color::Color(ARGB value): BaseColor(value) {}

ARGB Color::to_argb()
{
    ARGB value = 255;
    value <<= 8;
    value += red;
    value <<= 8;
    value += green;
    value <<= 8;
    value += blue;
    return value;
}

Color Color::operator +=(const Color &other)
{
    int tmp = this->red + other.red;
    this->red = (tmp > 255? 255: tmp);
    tmp = this->green + other.green;
    this->green = (tmp > 255? 255: tmp);
    tmp = this->blue + other.blue;
    this->blue = (tmp > 255? 255: tmp);
    return *this;
}

#define EPS 1e-10
Color Color::operator *=(double mul)
{
//        ToDo : find a better way!
    unsigned tmp;
    tmp = red * mul;
    if (tmp > 255)
    {
        unsigned adder = (tmp - 255) / 2;
        red = 255;
        blue = min<unsigned>(255, blue + adder);
        green = min<unsigned>(255, green + adder);
    }
    else
        red *= mul;
    tmp = blue * mul;
    if (tmp > 255)
    {
        unsigned adder = (tmp - 255) / 2;
        blue = 255;
        red = min<unsigned>(255, red + adder);
        green = min<unsigned>(255, green + adder);
    }
    else
        blue *= mul;
    tmp = green * mul;
    if (tmp > 255)
    {
        unsigned adder = (tmp - 255) / 2;
        green = 255;
        blue = min<unsigned>(255, blue + adder);
        red = min<unsigned>(255, red + adder);
    }
    else
        green *= mul;
    return *this;
}
#undef EPS

Color operator * (const Color &color, double mul)
{
    Color res(color);
    res *= mul;
    return res;
}

Color operator + (const Color &c1, const Color &c2)
{
    Color sum(c1);
    sum += c2;
    return sum;
}


TransparentColor::TransparentColor(ARGB value): BaseColor(value)
{
    value >>= 24;
    opacity = value % 256; // % 256 is probuably not needed here but is safer
}

ARGB TransparentColor::to_argb()
{
    ARGB value = opacity;
    value <<= 8;
    value += red;
    value <<= 8;
    value += green;
    value <<= 8;
    value += blue;
    return value;
}

TransparentColor TransparentColor::operator +=(const TransparentColor &other)
{
    int tmp = (this->red * this->opacity + other.red * other.opacity) / 255;
    this->red = (tmp > 255? 255: tmp);
    tmp = (this->green * this->opacity + other.green * other.opacity) / 255;
    this->green = (tmp > 255? 255: tmp);
    tmp = (this->blue * this->opacity + other.blue * other.opacity) / 255;
    this->blue = (tmp > 255? 255: tmp);
    this->opacity = 255 - (255 - this->opacity) * (255 - other.opacity);
    return *this;
}

TransparentColor TransparentColor::operator *=(double mul)
{
//    red = multiplier_gradient(red * mul / 255) * 255;
//    green = multiplier_gradient(green * mul / 255) * 255;
//    double tmp = multiplier_gradient(blue * mul / 255);
//    blue = tmp * 255;
//        ToDo : find a better way!
    if (red != 0)
    {
        if (255/red < mul)
            red = 255;
        else
            red *= mul;
    }
    if (blue != 0)
    {
        if (255/blue < mul)
            blue = 255;
        else
            blue *= mul;
    }
    if (green != 0)
    {
        if (255/green < mul)
            green = 255;
        else
            green *= mul;
    }
    return *this;
}

TransparentColor operator * (const TransparentColor &color, double mul)
{
    TransparentColor res(color);
    res *= mul;
    return res;
}

TransparentColor operator + (const TransparentColor &c1, const TransparentColor &c2)
{
    TransparentColor sum(c1);
    sum += c2;
    return sum;
}
