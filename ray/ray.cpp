#include <limits>
#include <cmath>
#include "ray.h"
#define EPS 1e-6

Ray::Ray(const Line &dir): dir(dir) {}

Color Ray::run_body(const Scene &scene, double min_render_distance, unsigned char recursion_limit)
{
    unsigned size = scene.size();
    double tmin = -1, brilliance = -1, reflectivity = -1, transperancy = 0;
    bool tmin_is_undefined = true;
    Color colormin = scene.background();
    Point normalmin;
    unsigned imin = size;
    for (unsigned i = 0; i < size; ++i)
    {
        Color color;
        Point normal;
        double t, brill, refl, transp;
        bool found = scene[i]->find(t, color, normal, brill, refl, transp, *this, min_render_distance);
        if (found && (tmin_is_undefined || t < tmin))
        {
            tmin = t;
            colormin = color;
            normalmin = normal;
            imin = i;
            brilliance = brill;
            reflectivity = refl;
            transperancy = transp;
            tmin_is_undefined = false;
        }
    }
    if (imin < size)
    {
        double intensity = scene.lights.ambient->intensity();
        Point intersection = point_at(tmin);
        for (DirectLight *light: scene.lights.direct)
        {
            Ray shader(Line(intersection, intersection - light->direction()));
            double passed_light = 1, tmp;
            constexpr double double_max = std::numeric_limits<double>::max();
            for (unsigned i = 0; i < size && passed_light > 0; ++i)
            {
                if (scene[i]->has_intersection(tmp, shader, EPS, double_max))
                    passed_light *= tmp;
            }
            if (passed_light > 0)
            {
                intensity += light->intensity(normalmin) * passed_light;
                Point light_direction = light->direction();
                if (brilliance > 0 && (light_direction * normalmin < 0))
                {
                    Point reflected = 2 * normalmin * (normalmin * light_direction) - light_direction;
                    double devitation = reflected * dir.RVector();
                    if (devitation > 0)
                        intensity += light->base_intensity() * passed_light * pow(devitation / (reflected.length() * dir.RVector().length()), brilliance);
                }
            }
        }
        for (PointLight *light: scene.lights.point)
        {
            Ray shader(Line(intersection, light->source_point()));
            double passed_light = 1, tmp;
            double lightpoint_dist = (light->source_point() - intersection).length();
            for (unsigned i = 0; i < size && passed_light > 0; ++i)
            {
                if (scene[i]->has_intersection(tmp, shader, EPS, lightpoint_dist))
                    passed_light *= tmp;
            }
            if (passed_light > 0)
            {
                intensity += light->intensity_at(intersection, normalmin) * passed_light;
                Point light_direction = intersection - light->source_point();
                if (brilliance > 0 && (light_direction * normalmin < 0))
                {
                    Point reflected = 2 * normalmin * (normalmin * light_direction) - light_direction;
                    double deviation = reflected * dir.RVector();
                    if (deviation > 0)
                        intensity += light->base_intensity() * passed_light * pow(deviation / (reflected.length() * dir.RVector().length()), brilliance);
                }
            }
        }
        colormin = colormin * intensity;
        if (recursion_limit && reflectivity > 0)
        {
            Point reflected_dir = -(2 * normalmin * (normalmin * dir.RVector()) - dir.RVector());
            Ray reflected(Line(intersection, intersection + reflected_dir));
            Color reflected_color = reflected.run_body(scene, EPS, recursion_limit - 1);
            colormin = colormin * (1 - reflectivity) + reflected_color * reflectivity;
        }
        if (recursion_limit && transperancy > 0)
        {
            Point second = 2 * intersection - dir.first();
            Ray passed(Line(intersection, second));
            Color passed_color = passed.run_body(scene, EPS, recursion_limit - 1);
            colormin = colormin * (1 - transperancy) + passed_color * transperancy;
        }

    }
    return colormin;
}

bool Ray::intersects_plane(double &result, const Point &plane_first, const Point &plane_second, const Point &plane_third) const
{
    Point plane_normal = (plane_first - plane_second) ^ (plane_first - plane_third);
    Point line_vector = dir.RVector();
    double dist, dot_product;
    bool intersects;

    plane_normal /= plane_normal.length();

    dist = plane_normal * (plane_first - dir.first());
    dot_product = plane_normal * line_vector;

    if (dot_product)
    {
        result = dist / dot_product;
        intersects = true;
    }
    else
        intersects = false;
    return intersects;
}
