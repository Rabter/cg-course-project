#ifndef RAY_H
#define RAY_H

#include "color/color.h"
#include "scene/scene.h"
#include "geometry/geometric_primitives.h"
#define RECURSION_LIMIT 3

class Scene;

class Ray
{
public:
    Ray(const Line &dir);
    inline ARGB run(const Scene &scene, double screen_distance)
    { return run_body(scene, screen_distance, RECURSION_LIMIT).to_argb(); }
    inline Line direction() const
    { return dir; }

    bool intersects_plane(double &result, const Point &plane_first, const Point &plane_second, const Point &plane_third) const;
    inline Point point_at(double t_value) const
    { return dir.first() + dir.RVector() * t_value; }

protected:
    Line dir;
    Color run_body(const Scene &scene, double min_render_distance, unsigned char recursion_limit = 3);
};

#undef RECURSION_LIMIT
#endif // RAY_H
