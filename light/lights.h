#ifndef LIGHTS_H
#define LIGHTS_H

#include <vector>
#include "ambient_light.h"
#include "direct_light.h"
#include "point_light.h"

struct Lights
{
    inline Lights() { ambient = AmbientLight::instance(); }
    AmbientLight *ambient;
    std::vector<DirectLight*> direct;
    std::vector<PointLight*> point;
};

#endif // LIGHTS_H
