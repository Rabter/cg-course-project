#ifndef DIRECTLIGHT_H
#define DIRECTLIGHT_H

#include "geometry/geometric_primitives.h"

class DirectLight
{
public:
    DirectLight(const Point &_direction, double intesity = 0);
    double intensity(const Point &normal) const;
    inline double base_intensity() const { return _base_intensity; }
    inline void set_direction(const Point &new_direction) { _direction = new_direction; }
    inline void set_intensity(double new_intensity) { _base_intensity = new_intensity; }
    inline Point direction() const { return _direction; }

    void rotate_x(double angle);
    void rotate_y(double angle);
    void rotate_z(double angle);

private:
    Point _direction;
    double _base_intensity;
};

#endif // DIRECTLIGHT_H
