#ifndef AMBIENT_LIGHT_H
#define AMBIENT_LIGHT_H

// Singleton
class AmbientLight
{
public:
    static AmbientLight* instance(double intensity = 0);
    static void delete_instance();
    inline double intensity() const { return base_intensity; }

protected:
    static AmbientLight *self;
    double base_intensity;
    AmbientLight(double intensity);
    virtual ~AmbientLight() {}
};

#endif // AMBIENT_LIGHT_H
