#include "direct_light.h"

DirectLight::DirectLight(const Point &direction, double intesity): _direction(direction), _base_intensity(intesity)
{
    _direction = _direction / _direction.length();
}

double DirectLight::intensity(const Point &normal) const
{
    double cosinus = - (_direction * normal) / (_direction.length() * normal.length());
    if (cosinus < 0)
        return 0;
    return _base_intensity * cosinus;
}

void DirectLight::rotate_x(double angle)
{
    _direction.rotate_x(angle);
}

void DirectLight::rotate_y(double angle)
{
    _direction.rotate_y(angle);
}

void DirectLight::rotate_z(double angle)
{
    _direction.rotate_z(angle);
}
