#ifndef POINT_LIGHT_H
#define POINT_LIGHT_H

#include "geometry/geometric_primitives.h"

class PointLight
{
public:
    PointLight(const Point &_source_point, double base_intensity = 0);
    double intensity_at(const Point &point, const Point &normal) const;
    inline double base_intensity() const { return _base_intensity; }
    inline Point source_point() const { return _source_point; }

    inline void set_intensity(double new_intensity) { _base_intensity = new_intensity; }

    void move(const Point &shift);
    void rotate_x(double angle);
    void rotate_y(double angle);
    void rotate_z(double angle);

protected:
    Point _source_point;
    double _base_intensity;
};

#endif // POINT_LIGHT_H
