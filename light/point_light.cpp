#include "point_light.h"

PointLight::PointLight(const Point &source_point, double intensity): _source_point(source_point), _base_intensity(intensity) {}

double PointLight::intensity_at(const Point &point, const Point &normal) const
{
    Point dir = point - _source_point;
    double cosinus = -(dir * normal) / (dir.length() * normal.length());
    if (cosinus < 0)
        return 0;
    return _base_intensity * cosinus;
}

void PointLight::move(const Point &shift)
{
    _source_point += shift;
}

void PointLight::rotate_x(double angle)
{
    _source_point.rotate_x(angle);
}

void PointLight::rotate_y(double angle)
{
    _source_point.rotate_y(angle);
}

void PointLight::rotate_z(double angle)
{
    _source_point.rotate_z(angle);
}

