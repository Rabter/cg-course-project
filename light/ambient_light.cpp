#include "ambient_light.h"

AmbientLight *AmbientLight::instance(double intensity)
{
    if(self)
        self->base_intensity += intensity;
    else
        self = new AmbientLight(intensity);
    return self;
}

void AmbientLight::delete_instance()
{
    if(self)
    {
        delete self;
        self = 0;
    }
}

AmbientLight::AmbientLight(double intensity): base_intensity(intensity) {}

AmbientLight* AmbientLight ::self = nullptr;
